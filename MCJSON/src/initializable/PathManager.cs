﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCJSON.src
{
    public class PathManager : Initializable
    {

        public static string modFolderPath { get; set; }

        public static string assetsFolder { get { return modFolderPath + "\\src\\main\\resources\\assets\\"; } }

        public static string itemModelsFolder { get { return modFolderPath != "" ? assetsFolder + Manager.MODID + "\\models\\item\\" : ""; } }

        public static string blockModelsFolder { get { return modFolderPath != "" ? assetsFolder + Manager.MODID + "\\models\\block\\" : ""; } }

        public static string blockstatesFolder { get { return modFolderPath != "" ? assetsFolder + Manager.MODID + "\\blockstates\\" : ""; } }

        public static string GetItemPath(string itemName)
        {
            return itemModelsFolder + itemName + ".json";
        }

        public static string GetBlockPath(string blockName)
        {
            return blockModelsFolder + blockName + ".json";
        }

        public static string GetBlockstatePath(string blockstateName)
        {
            return blockstatesFolder + blockstateName + ".json";
        }

        public static void ClearModFolderPath()
        {
            modFolderPath = "";
        }

        public static void ManageFoldersOnClick(FolderBrowserDialog modFolderDialog)
        {
            if (modFolderDialog.ShowDialog() == DialogResult.OK)
            {
                if (modFolderDialog.SelectedPath != "")
                {
                    modFolderPath = modFolderDialog.SelectedPath;

                    InitFolders(App.pTextModFolder);
                }
            }
        }

        public new static void Init()
        {
            modFolderPath = Properties.Settings.Default.lastFolder;
            if (modFolderPath != "")
            {
                InitFolders(App.pTextModFolder);
            }
        }

        private static void InitFolders(Label textModFolder)
        {

            if (!Directory.Exists(assetsFolder))
            {
                Directory.CreateDirectory(assetsFolder);
            }

            string[] subDirs = Directory.GetDirectories(assetsFolder, "*", SearchOption.TopDirectoryOnly);

            if (subDirs.Length > 0)
            {
                int diffLength = subDirs[0].Length - assetsFolder.Length;

                /* On récupère le modid du mod */

                Manager.MODID = subDirs[0].Remove(0, subDirs[0].Length - diffLength);
                //Console.WriteLine("MODID: " + PathManager.blockstatesFolder);
                App.ActiveForm.Text = Utils.APP_NAME + " | MODID: " + Manager.MODID + " | " + modFolderPath;

                /*
                    Création des dossiers models et blockstates 
                */

                Console.WriteLine("MODID : " + Manager.MODID);

                Directory.CreateDirectory(itemModelsFolder);
                Directory.CreateDirectory(blockModelsFolder);
                Directory.CreateDirectory(blockstatesFolder);

                Console.WriteLine("Mod folder has been chosen : \"" + modFolderPath + "\"");

                Properties.Settings.Default.lastFolder = modFolderPath;

            }
            else
            {
                App.ActiveForm.Text = Utils.APP_NAME;
                MessageBox.Show("Impossible de trouver votre modid !\nCréez bien un dossier dans assets ayant le nom exact de votre modid !", "Erreur");
                modFolderPath = Properties.Settings.Default.lastFolder;
                return;
            }
            textModFolder.Text = "";
        }



    }
}
