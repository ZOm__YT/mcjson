﻿using MCJSON.src.FileTypes.Blocks;
using MCJSON.src.handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCJSON.src
{
    public class Manager : Initializable
    {
        public static string MODID { get; set; }

        public static JsonTypes currentType { get; set; }

        public static IDictionary<string, Item> ObjectsToGenerate { get; set; }

        public static string lastItem { get; private set; }

        public static void InitAll()
        {
            Utils.Init();
            Init();
            PathManager.Init();
            
        }

        private static new void Init()
        {
            currentType = JsonTypes.NONE;
            ObjectsToGenerate = new Dictionary<string, Item>();
            MODID = "";
        }

        public static void GenerateModels(CheckBox outputCheck, TextBox namesBox)
        {
            if (ObjectsToGenerate.Count > 0)
            {
                foreach (Item item in ObjectsToGenerate.Values)
                {
                    item.GenerateFiles();
                }
                MessageBox.Show("Génération terminée !", "Réussi !");
                if (outputCheck.Checked)
                {
                    /* Ouverture des dossiers */
                    Process.Start("explorer.exe", PathManager.assetsFolder + MODID + "\\");
                }

                ObjectsToGenerate.Clear();
                updateNamesBox(namesBox);

                outputCheck.Checked = false;
            }
            else
            {
                MessageBox.Show("Vous n'avez pas ajouté d'item à générer !", "Attention !");
            }
        }

        public static void updateNamesBox(TextBox namesBox)
        {
            namesBox.MinimumSize = new Size(380, 20 * (ObjectsToGenerate.Count + 1));
            namesBox.Size = namesBox.MinimumSize;
            namesBox.Clear();
            foreach (Item item in ObjectsToGenerate.Values)
            {
                Console.WriteLine(item);
                if (item is Block b)
                {
                    if (b is BlockCrops bc)
                    {
                        namesBox.Text += "[" + bc.JsonType.ToString().ToUpper() + " ]\"" + bc.Name.ToLower() + ".json\" [ \"" + bc.BVariant.ToString().ToUpper() + "\" ] [ MAX-AGE: " + bc.MaxAge + " ]" + Environment.NewLine;
                        lastItem = bc.Name;
                        continue;
                    }
                    namesBox.Text += "[" + b.JsonType.ToString().ToUpper() + " ]\"" + b.Name.ToLower() + ".json\" [ \"" + b.BVariant.ToString().ToUpper() + "\" ]" + Environment.NewLine;
                    lastItem = b.Name;
                    continue;
                }
                namesBox.Text += "[" + item.JsonType.ToString().ToUpper() + "] \"" + item.Name.ToLower() + ".json\" [ \"" + item.IVariant.ToString().ToUpper() + "\" ]" + Environment.NewLine;
                lastItem = item.Name;
            }

        }

        public static void SelectIcon(int id, PictureBox itemIcon, PictureBox blockIcon)
        {
            switch (id)
            {
                case 0:
                    if (currentType == JsonTypes.ITEM) return;
                    currentType = JsonTypes.ITEM;
                    itemIcon.Image = Utils.Icons[0];
                    blockIcon.Image = Utils.Icons[3];
                    break;

                case 2:
                    if (currentType == JsonTypes.BLOCK) return;
                    currentType = JsonTypes.BLOCK;
                    itemIcon.Image = Utils.Icons[1];
                    blockIcon.Image = Utils.Icons[2];
                    break;
            }
            Console.WriteLine(currentType.ToString());
        }
    }
}
