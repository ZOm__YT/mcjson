﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src
{
    public class Utils : Initializable
    {
        public Utils()
        {
        }

        public static Bitmap[] Icons { get; set; }

        public static string APP_NAME { get { return "MCJson by Zom'"; } }

        public string FormatJson(string file, string itemName)
        {
            return file.Replace("modid", Manager.MODID).Replace("itemName", itemName);
        }

        public static new void Init()
        {
            Icons = new Bitmap[]{ Properties.Resources.apple, Properties.Resources.gApple,
                Properties.Resources.grass,Properties.Resources.gGrass};

        }


    }
}
