﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src
{
    public enum BlockVariants
    {
        NONE, CUBE_ALL, CUBE_COLUMN, DOOR, STAIRS, SLAB, CROPS
    }
}
