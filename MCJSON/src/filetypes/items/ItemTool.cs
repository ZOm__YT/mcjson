﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src.FileTypes.Items
{
    public class ItemTool : Item
    {
        public ItemTool(string name) : base(name)
        {
            IVariant = ItemVariants.HANDHELD;
            Model = Properties.Resources.toolJsonTemplate;
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();
        }
    }
}
