﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src
{
    public class Item
    {

        public string Name { get; protected set; }

        public JsonTypes JsonType { get; set; }
        
        public ItemVariants IVariant { get; set; }

        public BlockVariants BVariant { get; set; }

        protected string Model { get; set; }

        public Item(string name)
        {
            Name = name;
            JsonType = JsonTypes.ITEM;
            IVariant = ItemVariants.GENERATED;
            BVariant = BlockVariants.NONE;
            Model = Properties.Resources.itemJsonTemplate;
        }

        public virtual void GenerateFiles()
        {
            string newFile = Model.Replace("modid", Manager.MODID).Replace("itemName", Name);
            File.WriteAllText(PathManager.GetItemPath(Name), newFile);
        }

    }
}
