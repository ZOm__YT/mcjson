﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src.FileTypes.Blocks
{
    public class BlockStairs : Block
    {
        public BlockStairs(string blockName) : base(blockName)
        {
            BVariant = BlockVariants.STAIRS;
            BlockModels.Clear();
            BlockModels.Add(Name, Properties.Resources.stairs_model);
            BlockModels.Add(Name + "_inner", Properties.Resources.stairs_inner_model);
            BlockModels.Add(Name + "_outer", Properties.Resources.stairs_outer_model);
            BlockstateModel = Properties.Resources.stairs_blockstate;
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();  
        }
    }
}
