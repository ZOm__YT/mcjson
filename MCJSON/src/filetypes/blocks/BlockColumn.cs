﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src
{
    public class BlockColumn : Block
    {

        public BlockColumn(string blockName) : base(blockName)
        {
            BVariant = BlockVariants.CUBE_COLUMN;
            BlockModels[Name] = Properties.Resources.block_column_model;
            BlockstateModel = Properties.Resources.block_column_blockstates;
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();
        }

    }
}
