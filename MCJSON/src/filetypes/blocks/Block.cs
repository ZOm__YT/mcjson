﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src
{
    public class Block : Item
    {

        /* Key: Nom du fichier json, Value: Contenu du fichier */
        protected Dictionary<string, string> BlockModels { get; set; }

        protected string BlockItemModel { get; set; }

        protected string BlockstateModel { get; set; }

        public Block(string blockName) : base(blockName)
        {
            JsonType = JsonTypes.BLOCK;
            BVariant = BlockVariants.CUBE_ALL;
            IVariant = ItemVariants.NONE;
            BlockModels = new Dictionary<string, string>();
            BlockModels.Add(Name, Properties.Resources.blockJsonTemplate);
            BlockItemModel = Properties.Resources.itemBlockJsonTemplate;
            BlockstateModel = Properties.Resources.blockstateJsonTemplate;
        }

        public override void GenerateFiles()
        {
            foreach(string key in BlockModels.Keys)
            {
                string newFile = BlockModels[key].Replace("modid", Manager.MODID).Replace("itemName", Name);

                File.WriteAllText(PathManager.GetBlockPath(key), newFile);
            }
            string itemFile = BlockItemModel.Replace("modid", Manager.MODID).Replace("itemName", Name);
            string blockstateFile = BlockstateModel.Replace("modid", Manager.MODID).Replace("itemName", Name);

            File.WriteAllText(PathManager.GetItemPath(Name), itemFile);
            File.WriteAllText(PathManager.GetBlockstatePath(Name), blockstateFile);
        }
    }
}
