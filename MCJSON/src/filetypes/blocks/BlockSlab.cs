﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src.FileTypes.Blocks
{
    public class BlockSlab : Block
    {
        public BlockSlab(string blockName) : base(blockName)
        {
            BVariant = BlockVariants.SLAB;
            BlockModels.Clear();
            BlockModels.Add(Name, Properties.Resources.slab_block);
            BlockModels.Add(Name + "_top", Properties.Resources.slab_block_top);
            BlockstateModel = Properties.Resources.slab_blockstate;
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();
        }
    }
}
