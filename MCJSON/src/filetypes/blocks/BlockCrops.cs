﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src.FileTypes.Blocks
{
    public class BlockCrops : Block
    {

        public int MaxAge { get; private set; }

        public BlockCrops(string blockName, int maxAge) : base(blockName)
        {
            MaxAge = maxAge;
            BVariant = BlockVariants.CROPS;
            BlockstateModel = Properties.Resources.crop_blockstate;
            BlockModels.Clear();
            for (int i = 0; i <= MaxAge; i++)
            {
                BlockModels.Add(Name + "_stage" + i, Properties.Resources.crop_block_model);
                if (i == MaxAge)
                {
                    BlockstateModel += "\n     \"age=" + i + "\": {\"model\":\"modid:block/itemName_stage" + i + "\"}";
                    BlockstateModel += "\n   }\n}";
                    break;
                }
                BlockstateModel += "\n     \"age=" + i + "\": {\"model\":\"modid:block/itemName_stage" + i + "\"},";
            }
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();
        }
    }
}
