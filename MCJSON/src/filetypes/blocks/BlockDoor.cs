﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCJSON.src.FileTypes.Blocks
{
    public class BlockDoor : Block
    {
        public BlockDoor(string blockName) : base(blockName)
        {
            BVariant = BlockVariants.DOOR;
            BlockModels.Clear();
            BlockModels.Add(Name + "_top", Properties.Resources.door_top_model);
            BlockModels.Add(Name + "_top_hinge", Properties.Resources.door_top_hinge_model);
            BlockModels.Add(Name + "_bottom", Properties.Resources.door_bottom_model);
            BlockModels.Add(Name + "_bottom_hinge", Properties.Resources.door_bottom_hinge_model);
            BlockstateModel = Properties.Resources.door_blockstate;
            BlockItemModel = Properties.Resources.itemJsonTemplate;
        }

        public override void GenerateFiles()
        {
            base.GenerateFiles();
        }
    }
}
