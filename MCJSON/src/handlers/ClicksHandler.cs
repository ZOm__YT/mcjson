﻿using MCJSON.src.FileTypes.Blocks;
using MCJSON.src.FileTypes.Items;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCJSON.src.handlers
{
    public class ClicksHandler
    {

        public static void addObject(TextBox fileNameInput, ComboBox itemVariantsDropdown, ComboBox blockVariantsDropdown, TextBox cropsAgeBox, TextBox namesBox)
        {
            if (Manager.currentType == JsonTypes.NONE)
            {
                MessageBox.Show("Vous devez sélectionner un type de Json !", "Erreur !");
                return;
            }

            // On check si le champ du nom des objets contient des caractères
            if (fileNameInput.Text != "")
            {

                string objectName = fileNameInput.Text.ToLower();
                if (Manager.ObjectsToGenerate.ContainsKey(objectName))
                {
                    return;
                }
                switch (Manager.currentType)
                {
                    case JsonTypes.NONE:
                        MessageBox.Show("Vous n'avez pas sélectionné le type de Json", "Erreur");
                        break;
                    case JsonTypes.ITEM:
                        if (itemVariantsDropdown.Text != "")
                        {
                            switch (itemVariantsDropdown.Text.ToLower())
                            {
                                case "generated":
                                    Manager.ObjectsToGenerate.Add(objectName, new Item(objectName));
                                    break;
                                case "handheld":
                                    Manager.ObjectsToGenerate.Add(objectName, new ItemTool(objectName));
                                    break;
                            }
                        }
                        break;
                    //TODO Génération des fichiers
                    case JsonTypes.BLOCK:
                        if (blockVariantsDropdown.Text != "")
                        {
                            switch (blockVariantsDropdown.Text.ToLower())
                            {
                                case "cube_all":
                                    Manager.ObjectsToGenerate.Add(objectName, new Block(objectName));
                                    break;

                                case "cube_column":
                                    Manager.ObjectsToGenerate.Add(objectName, new BlockColumn(objectName));
                                    break;

                                case "door":
                                    Manager.ObjectsToGenerate.Add(objectName, new BlockDoor(objectName));
                                    break;

                                case "stairs":
                                    Manager.ObjectsToGenerate.Add(objectName, new BlockStairs(objectName));
                                    break;

                                case "slab":
                                    Manager.ObjectsToGenerate.Add(objectName, new BlockSlab(objectName));
                                    break;

                                case "crops":
                                    if (!cropsAgeBox.Visible && !cropsAgeBox.Enabled) { return; }
                                    if (cropsAgeBox.Text == "")
                                    {
                                        MessageBox.Show("Vous devez spécifier l'âge maximal !", "Erreur");
                                        return;
                                    }
                                    Manager.ObjectsToGenerate.Add(objectName, new BlockCrops(objectName, int.Parse(cropsAgeBox.Text)));
                                    break;
                            }
                        }
                        break;
                }
                Manager.updateNamesBox(namesBox);
                fileNameInput.Text = "";
            }
        }



    }
}
