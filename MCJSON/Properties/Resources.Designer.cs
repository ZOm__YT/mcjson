﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCJSON.Properties {
    using System;
    
    
    /// <summary>
    ///   Une classe de ressource fortement typée destinée, entre autres, à la consultation des chaînes localisées.
    /// </summary>
    // Cette classe a été générée automatiquement par la classe StronglyTypedResourceBuilder
    // à l'aide d'un outil, tel que ResGen ou Visual Studio.
    // Pour ajouter ou supprimer un membre, modifiez votre fichier .ResX, puis réexécutez ResGen
    // avec l'option /str ou régénérez votre projet VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Retourne l'instance ResourceManager mise en cache utilisée par cette classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MCJSON.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Remplace la propriété CurrentUICulture du thread actuel pour toutes
        ///   les recherches de ressources à l'aide de cette classe de ressource fortement typée.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap apple {
            get {
                object obj = ResourceManager.GetObject("apple", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;variants&quot;: {
        ///        &quot;axis=y&quot;:  { &quot;model&quot;: &quot;modid:itemName&quot; },
        ///        &quot;axis=z&quot;:   { &quot;model&quot;: &quot;modid:itemName&quot;, &quot;x&quot;: 90 },
        ///        &quot;axis=x&quot;:   { &quot;model&quot;: &quot;modid:itemName&quot;, &quot;x&quot;: 90, &quot;y&quot;: 90 }
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string block_column_blockstates {
            get {
                return ResourceManager.GetString("block_column_blockstates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/cube_column&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;end&quot;: &quot;modid:blocks/itemName_top&quot;,
        ///        &quot;side&quot;: &quot;modid:blocks/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string block_column_model {
            get {
                return ResourceManager.GetString("block_column_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/cube_all&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;all&quot;: &quot;modid:block/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string blockJsonTemplate {
            get {
                return ResourceManager.GetString("blockJsonTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///	&quot;variants&quot;: {
        ///		&quot;&quot;: { &quot;model&quot;: &quot;modid:block/itemName&quot; }
        ///	}
        ///}
        ///.
        /// </summary>
        internal static string blockstateJsonTemplate {
            get {
                return ResourceManager.GetString("blockstateJsonTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/crop&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;crop&quot;: &quot;modid:block/crop_stage_cropAge&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string crop_block_model {
            get {
                return ResourceManager.GetString("crop_block_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;variants&quot;: {
        ///        
        ///
        ///.
        /// </summary>
        internal static string crop_blockstate {
            get {
                return ResourceManager.GetString("crop_blockstate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;variants&quot;: {
        ///        &quot;facing=east,half=lower,hinge=left,open=false&quot;:  { &quot;model&quot;: &quot;modid:itemName_bottom&quot; },
        ///        &quot;facing=south,half=lower,hinge=left,open=false&quot;: { &quot;model&quot;: &quot;modid:itemName_bottom&quot;, &quot;y&quot;: 90 },
        ///        &quot;facing=west,half=lower,hinge=left,open=false&quot;:  { &quot;model&quot;: &quot;modid:itemName_bottom&quot;, &quot;y&quot;: 180 },
        ///        &quot;facing=north,half=lower,hinge=left,open=false&quot;: { &quot;model&quot;: &quot;modid:itemName_bottom&quot;, &quot;y&quot;: 270 },
        ///        &quot;facing=east,half=lower,hinge=right,open=false&quot;:  { &quot;model&quot;: &quot;modid:itemNam [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string door_blockstate {
            get {
                return ResourceManager.GetString("door_blockstate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/door_bottom_rh&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:blocks/itemName_bottom&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/itemName_top&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string door_bottom_hinge_model {
            get {
                return ResourceManager.GetString("door_bottom_hinge_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/door_bottom&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:blocks/itemName_bottom&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/itemName_top&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string door_bottom_model {
            get {
                return ResourceManager.GetString("door_bottom_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/door_top_rh&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:blocks/itemName_bottom&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/itemName_top&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string door_top_hinge_model {
            get {
                return ResourceManager.GetString("door_top_hinge_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/door_top&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:blocks/itemName_bottom&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/itemName_top&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string door_top_model {
            get {
                return ResourceManager.GetString("door_top_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap gApple {
            get {
                object obj = ResourceManager.GetObject("gApple", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.IO.UnmanagedMemoryStream semblable à System.IO.MemoryStream.
        /// </summary>
        internal static System.IO.UnmanagedMemoryStream genCompleteSound {
            get {
                return ResourceManager.GetStream("genCompleteSound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap gGrass {
            get {
                object obj = ResourceManager.GetObject("gGrass", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap grass {
            get {
                object obj = ResourceManager.GetObject("grass", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap gSword {
            get {
                object obj = ResourceManager.GetObject("gSword", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.IO.UnmanagedMemoryStream semblable à System.IO.MemoryStream.
        /// </summary>
        internal static System.IO.UnmanagedMemoryStream imgClickSound {
            get {
                return ResourceManager.GetStream("imgClickSound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///	&quot;parent&quot;: &quot;modid:block/itemName&quot;
        ///}
        ///.
        /// </summary>
        internal static string itemBlockJsonTemplate {
            get {
                return ResourceManager.GetString("itemBlockJsonTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;item/generated&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;layer0&quot;: &quot;modid:item/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string itemJsonTemplate {
            get {
                return ResourceManager.GetString("itemJsonTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///	&quot;parent&quot;: &quot;block/slab&quot;,
        ///	&quot;textures&quot;: {
        ///		&quot;bottom&quot;: &quot;modid:block/itemName&quot;,
        ///		&quot;top&quot;: &quot;modid:block/itemName&quot;,
        ///		&quot;side&quot;: &quot;modid:block/itemName&quot;
        ///	}
        ///}
        ///.
        /// </summary>
        internal static string slab_block {
            get {
                return ResourceManager.GetString("slab_block", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/slab_top&quot;,
        ///	&quot;textures&quot;: {
        ///		&quot;bottom&quot;: &quot;modid:block/itemName&quot;,
        ///		&quot;top&quot;: &quot;modid:block/itemName&quot;,
        ///		&quot;side&quot;: &quot;modid:block/itemName&quot;
        ///	}
        ///}
        ///.
        /// </summary>
        internal static string slab_block_top {
            get {
                return ResourceManager.GetString("slab_block_top", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;variants&quot;: {
        ///      &quot;type=bottom&quot;: { &quot;model&quot;: &quot;modid:block/itemName_slab&quot; },
        ///			&quot;type=top&quot;: { &quot;model&quot;: &quot;modid:block/itemName_slab_top&quot; },
        ///			&quot;type=double&quot;: { &quot;model&quot;: &quot;modid:block/itemName&quot; }
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string slab_blockstate {
            get {
                return ResourceManager.GetString("slab_blockstate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;variants&quot;: {
        ///        &quot;facing=east,half=bottom,shape=straight&quot;:  { &quot;model&quot;: &quot;modid:itemName&quot; },
        ///        &quot;facing=west,half=bottom,shape=straight&quot;:  { &quot;model&quot;: &quot;modid:itemName&quot;, &quot;y&quot;: 180, &quot;uvlock&quot;: true },
        ///        &quot;facing=south,half=bottom,shape=straight&quot;: { &quot;model&quot;: &quot;modid:itemName&quot;, &quot;y&quot;: 90, &quot;uvlock&quot;: true },
        ///        &quot;facing=north,half=bottom,shape=straight&quot;: { &quot;model&quot;: &quot;modid:itemName&quot;, &quot;y&quot;: 270, &quot;uvlock&quot;: true },
        ///        &quot;facing=east,half=bottom,shape=outer_right&quot;:  { &quot;model&quot;: &quot;modid:itemName_outer&quot; [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string stairs_blockstate {
            get {
                return ResourceManager.GetString("stairs_blockstate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/inner_stairs&quot;,
        ///    &quot;textures&quot;: {
        ///			&quot;bottom&quot;: &quot;modid:blocks/itemName&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/itemName&quot;,
        ///        &quot;side&quot;: &quot;modid:blocks/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string stairs_inner_model {
            get {
                return ResourceManager.GetString("stairs_inner_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/stairs&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:blocks/acacia_planks&quot;,
        ///        &quot;top&quot;: &quot;modid:blocks/acacia_planks&quot;,
        ///        &quot;side&quot;: &quot;modid:blocks/acacia_planks&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string stairs_model {
            get {
                return ResourceManager.GetString("stairs_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;block/outer_stairs&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;bottom&quot;: &quot;modid:block/itemName&quot;,
        ///        &quot;top&quot;: &quot;modid:block/itemName&quot;,
        ///        &quot;side&quot;: &quot;modid:block/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string stairs_outer_model {
            get {
                return ResourceManager.GetString("stairs_outer_model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sword {
            get {
                object obj = ResourceManager.GetObject("sword", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à {
        ///    &quot;parent&quot;: &quot;item/handheld&quot;,
        ///    &quot;textures&quot;: {
        ///        &quot;layer0&quot;: &quot;modid:item/itemName&quot;
        ///    }
        ///}
        ///.
        /// </summary>
        internal static string toolJsonTemplate {
            get {
                return ResourceManager.GetString("toolJsonTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap yt_logo {
            get {
                object obj = ResourceManager.GetObject("yt_logo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
