﻿using MCJSON.src;
using MCJSON.src.handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MCJSON
{
    public partial class App : Form
    {

        public static Label pTextModFolder;

        public App()
        {
            InitializeComponent();
            pTextModFolder = textModFolder;
        }

        private void AddFileButton_Click(object sender, EventArgs e)
        {
            ClicksHandler.addObject(fileNameInput, itemVariantsBox, blockVariantsBox, cropTextBox, namesBox);
        }

        private void FolderButton_Click(object sender, EventArgs e)
        {
            PathManager.ManageFoldersOnClick(modFolderDialog);
        }

        private void ItemVariantsBox_TextChanged(object sender, EventArgs e)
        {
            if (!itemVariantsBox.Items.Contains(itemVariantsBox.Text)) itemVariantsBox.Text = itemVariantsBox.Items[0].ToString();
        }

        private void BlockVariantsBox_TextChanged(object sender, EventArgs e)
        {
            if (!blockVariantsBox.Items.Contains(blockVariantsBox.Text)) blockVariantsBox.Text = blockVariantsBox.Items[0].ToString();
        }

        private void GenButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("MODID GEN : " + Manager.MODID);
            if (PathManager.modFolderPath != "" && Manager.MODID != "")
            {
                Manager.GenerateModels(outputFolderCheck, namesBox);
            }else
            {
                MessageBox.Show("Vous n'avez pas sélectionné le dossier de votre mod !", "Erreur !");
            }
        }

        private void AppleIcon_Click(object sender, EventArgs e)
        {
            Manager.SelectIcon(0, appleIcon, grassIcon);
            blockVariantsBox.Visible = false;
            itemVariantsBox.Visible = true;
        }

        private void GrassIcon_Click(object sender, EventArgs e)
        {
            Manager.SelectIcon(2, appleIcon, grassIcon);
            blockVariantsBox.Visible = true;
            itemVariantsBox.Visible = false;
        }

        private void FileNameInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ClicksHandler.addObject(fileNameInput, itemVariantsBox, blockVariantsBox, cropTextBox, namesBox);
            }
        }

        private void BlockVariantsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (blockVariantsBox.Text == "Crops")
            {
                cropTextBox.Visible = true;
                cropTextBox.Enabled = true;
                maxStageLabel.Visible = true;
                cropTextBox.Text = "2";
            }
            else
            {
                cropTextBox.Visible = false;
                cropTextBox.Enabled = false;
                maxStageLabel.Visible = false;
            }

        }

        private void RemoveItemButton_Click(object sender, EventArgs e)
        {
            if (Manager.ObjectsToGenerate.Count > 0)
            {
                if (Control.ModifierKeys == Keys.Shift)
                {
                    Manager.ObjectsToGenerate.Clear();
                    Manager.updateNamesBox(namesBox);
                    return;
                }

                Manager.ObjectsToGenerate.Remove(Manager.lastItem);
                Manager.updateNamesBox(namesBox);
                Console.WriteLine(Manager.lastItem + " remove !");
            }

        }

        private void RemoveItemButton_MouseLeave(object sender, EventArgs e)
        {
            removeItemButton.Text = "Enlever le dernier fichier";
        }

        private void RemoveItemButton_MouseHover(object sender, MouseEventArgs e)
        {
            if (Control.ModifierKeys == Keys.Shift)
            {
                removeItemButton.Text = "Enlever tous les fichiers";
            }
            else
            {
                removeItemButton.Text = "Enlever le dernier fichier";
            }
        }

        private void YtButton_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.youtube.com/channel/UCdjw-3EI4xgOe341MJEiYmg?sub_confirmation=1");
        }

        private void App_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Save();
            Console.WriteLine("Preferences Saved !");
        }

        private void App_Shown(object sender, EventArgs e)
        {
            Manager.InitAll();
        }
    }
}
