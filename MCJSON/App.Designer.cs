﻿namespace MCJSON
{
    partial class App
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(App));
            this.folderButton = new System.Windows.Forms.Button();
            this.fileNameInput = new System.Windows.Forms.TextBox();
            this.modFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.addFileButton = new System.Windows.Forms.Button();
            this.textNamesBox = new System.Windows.Forms.Label();
            this.namesBox = new System.Windows.Forms.TextBox();
            this.textModFolder = new System.Windows.Forms.Label();
            this.genButton = new System.Windows.Forms.Button();
            this.appleIcon = new System.Windows.Forms.PictureBox();
            this.grassIcon = new System.Windows.Forms.PictureBox();
            this.genCompleteText = new System.Windows.Forms.Label();
            this.textModid = new System.Windows.Forms.Label();
            this.ytButton = new System.Windows.Forms.Button();
            this.blockVariantsBox = new System.Windows.Forms.ComboBox();
            this.removeItemButton = new System.Windows.Forms.Button();
            this.outputFolderCheck = new System.Windows.Forms.CheckBox();
            this.cropTextBox = new System.Windows.Forms.TextBox();
            this.maxStageLabel = new System.Windows.Forms.Label();
            this.itemVariantsBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.appleIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grassIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // folderButton
            // 
            this.folderButton.Location = new System.Drawing.Point(49, 91);
            this.folderButton.Name = "folderButton";
            this.folderButton.Size = new System.Drawing.Size(138, 23);
            this.folderButton.TabIndex = 0;
            this.folderButton.Text = "Choisir le dossier du mod";
            this.folderButton.UseVisualStyleBackColor = true;
            this.folderButton.Click += new System.EventHandler(this.FolderButton_Click);
            // 
            // fileNameInput
            // 
            this.fileNameInput.AllowDrop = true;
            this.fileNameInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.fileNameInput.Location = new System.Drawing.Point(28, 22);
            this.fileNameInput.Name = "fileNameInput";
            this.fileNameInput.Size = new System.Drawing.Size(175, 20);
            this.fileNameInput.TabIndex = 1;
            this.fileNameInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FileNameInput_KeyDown);
            // 
            // addFileButton
            // 
            this.addFileButton.Location = new System.Drawing.Point(69, 48);
            this.addFileButton.Name = "addFileButton";
            this.addFileButton.Size = new System.Drawing.Size(100, 37);
            this.addFileButton.TabIndex = 2;
            this.addFileButton.Text = "Ajouter un fichier";
            this.addFileButton.UseVisualStyleBackColor = true;
            this.addFileButton.Click += new System.EventHandler(this.AddFileButton_Click);
            // 
            // textNamesBox
            // 
            this.textNamesBox.AutoSize = true;
            this.textNamesBox.BackColor = System.Drawing.SystemColors.Control;
            this.textNamesBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.textNamesBox.Location = new System.Drawing.Point(517, 22);
            this.textNamesBox.MinimumSize = new System.Drawing.Size(150, 0);
            this.textNamesBox.Name = "textNamesBox";
            this.textNamesBox.Size = new System.Drawing.Size(150, 13);
            this.textNamesBox.TabIndex = 6;
            this.textNamesBox.Text = "Fichiers JSON à générer";
            this.textNamesBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // namesBox
            // 
            this.namesBox.Location = new System.Drawing.Point(405, 57);
            this.namesBox.MaximumSize = new System.Drawing.Size(500, 500);
            this.namesBox.MinimumSize = new System.Drawing.Size(150, 150);
            this.namesBox.Multiline = true;
            this.namesBox.Name = "namesBox";
            this.namesBox.ReadOnly = true;
            this.namesBox.Size = new System.Drawing.Size(366, 150);
            this.namesBox.TabIndex = 7;
            // 
            // textModFolder
            // 
            this.textModFolder.AutoEllipsis = true;
            this.textModFolder.AutoSize = true;
            this.textModFolder.Location = new System.Drawing.Point(12, 117);
            this.textModFolder.Name = "textModFolder";
            this.textModFolder.Size = new System.Drawing.Size(0, 13);
            this.textModFolder.TabIndex = 8;
            this.textModFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // genButton
            // 
            this.genButton.Location = new System.Drawing.Point(511, 271);
            this.genButton.Name = "genButton";
            this.genButton.Size = new System.Drawing.Size(165, 77);
            this.genButton.TabIndex = 10;
            this.genButton.Text = "Générer les fichiers";
            this.genButton.UseVisualStyleBackColor = true;
            this.genButton.Click += new System.EventHandler(this.GenButton_Click);
            // 
            // appleIcon
            // 
            this.appleIcon.Image = global::MCJSON.Properties.Resources.gApple;
            this.appleIcon.Location = new System.Drawing.Point(28, 317);
            this.appleIcon.Name = "appleIcon";
            this.appleIcon.Size = new System.Drawing.Size(70, 65);
            this.appleIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.appleIcon.TabIndex = 4;
            this.appleIcon.TabStop = false;
            this.appleIcon.Click += new System.EventHandler(this.AppleIcon_Click);
            // 
            // grassIcon
            // 
            this.grassIcon.Image = global::MCJSON.Properties.Resources.gGrass;
            this.grassIcon.Location = new System.Drawing.Point(28, 212);
            this.grassIcon.Name = "grassIcon";
            this.grassIcon.Size = new System.Drawing.Size(70, 65);
            this.grassIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.grassIcon.TabIndex = 3;
            this.grassIcon.TabStop = false;
            this.grassIcon.Click += new System.EventHandler(this.GrassIcon_Click);
            // 
            // genCompleteText
            // 
            this.genCompleteText.AutoSize = true;
            this.genCompleteText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.genCompleteText.Location = new System.Drawing.Point(517, 369);
            this.genCompleteText.Name = "genCompleteText";
            this.genCompleteText.Size = new System.Drawing.Size(0, 13);
            this.genCompleteText.TabIndex = 11;
            // 
            // textModid
            // 
            this.textModid.AutoSize = true;
            this.textModid.ForeColor = System.Drawing.Color.DodgerBlue;
            this.textModid.Location = new System.Drawing.Point(221, 406);
            this.textModid.Name = "textModid";
            this.textModid.Size = new System.Drawing.Size(0, 13);
            this.textModid.TabIndex = 12;
            this.textModid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ytButton
            // 
            this.ytButton.BackColor = System.Drawing.Color.Transparent;
            this.ytButton.BackgroundImage = global::MCJSON.Properties.Resources.yt_logo;
            this.ytButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ytButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ytButton.FlatAppearance.BorderSize = 0;
            this.ytButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro;
            this.ytButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.ytButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ytButton.ForeColor = System.Drawing.Color.Transparent;
            this.ytButton.Location = new System.Drawing.Point(734, 385);
            this.ytButton.Name = "ytButton";
            this.ytButton.Size = new System.Drawing.Size(54, 53);
            this.ytButton.TabIndex = 13;
            this.ytButton.UseVisualStyleBackColor = false;
            this.ytButton.Click += new System.EventHandler(this.YtButton_Click);
            // 
            // blockVariantsBox
            // 
            this.blockVariantsBox.FormattingEnabled = true;
            this.blockVariantsBox.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.blockVariantsBox.Items.AddRange(new object[] {
            "Cube_All",
            "Cube_Column",
            "Door",
            "Stairs",
            "Slab",
            "Crops"});
            this.blockVariantsBox.Location = new System.Drawing.Point(107, 232);
            this.blockVariantsBox.Name = "blockVariantsBox";
            this.blockVariantsBox.Size = new System.Drawing.Size(96, 21);
            this.blockVariantsBox.TabIndex = 14;
            this.blockVariantsBox.Text = "Cube_All";
            this.blockVariantsBox.Visible = false;
            this.blockVariantsBox.SelectedIndexChanged += new System.EventHandler(this.BlockVariantsBox_SelectedIndexChanged);
            this.blockVariantsBox.TextChanged += new System.EventHandler(this.BlockVariantsBox_TextChanged);
            // 
            // removeItemButton
            // 
            this.removeItemButton.Location = new System.Drawing.Point(292, 83);
            this.removeItemButton.Name = "removeItemButton";
            this.removeItemButton.Size = new System.Drawing.Size(94, 38);
            this.removeItemButton.TabIndex = 15;
            this.removeItemButton.Text = "Enlever le dernier fichier";
            this.removeItemButton.UseVisualStyleBackColor = true;
            this.removeItemButton.Click += new System.EventHandler(this.RemoveItemButton_Click);
            this.removeItemButton.MouseLeave += new System.EventHandler(this.RemoveItemButton_MouseLeave);
            this.removeItemButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RemoveItemButton_MouseHover);
            // 
            // outputFolderCheck
            // 
            this.outputFolderCheck.AutoSize = true;
            this.outputFolderCheck.Checked = true;
            this.outputFolderCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.outputFolderCheck.Location = new System.Drawing.Point(520, 354);
            this.outputFolderCheck.Name = "outputFolderCheck";
            this.outputFolderCheck.Size = new System.Drawing.Size(150, 17);
            this.outputFolderCheck.TabIndex = 16;
            this.outputFolderCheck.Text = "Ouvrir le dossier d\'export ?";
            this.outputFolderCheck.UseVisualStyleBackColor = true;
            // 
            // cropTextBox
            // 
            this.cropTextBox.Location = new System.Drawing.Point(210, 232);
            this.cropTextBox.Name = "cropTextBox";
            this.cropTextBox.Size = new System.Drawing.Size(49, 20);
            this.cropTextBox.TabIndex = 17;
            this.cropTextBox.Visible = false;
            // 
            // maxStageLabel
            // 
            this.maxStageLabel.AutoSize = true;
            this.maxStageLabel.Location = new System.Drawing.Point(201, 212);
            this.maxStageLabel.Name = "maxStageLabel";
            this.maxStageLabel.Size = new System.Drawing.Size(58, 13);
            this.maxStageLabel.TabIndex = 18;
            this.maxStageLabel.Text = "Stage Max";
            this.maxStageLabel.Visible = false;
            // 
            // itemVariantsBox
            // 
            this.itemVariantsBox.FormattingEnabled = true;
            this.itemVariantsBox.Items.AddRange(new object[] {
            "Generated",
            "Handheld"});
            this.itemVariantsBox.Location = new System.Drawing.Point(107, 340);
            this.itemVariantsBox.Name = "itemVariantsBox";
            this.itemVariantsBox.Size = new System.Drawing.Size(96, 21);
            this.itemVariantsBox.TabIndex = 19;
            this.itemVariantsBox.Text = "Generated";
            this.itemVariantsBox.TextChanged += new System.EventHandler(this.ItemVariantsBox_TextChanged);
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.itemVariantsBox);
            this.Controls.Add(this.maxStageLabel);
            this.Controls.Add(this.cropTextBox);
            this.Controls.Add(this.outputFolderCheck);
            this.Controls.Add(this.removeItemButton);
            this.Controls.Add(this.blockVariantsBox);
            this.Controls.Add(this.ytButton);
            this.Controls.Add(this.textModid);
            this.Controls.Add(this.genCompleteText);
            this.Controls.Add(this.genButton);
            this.Controls.Add(this.textModFolder);
            this.Controls.Add(this.namesBox);
            this.Controls.Add(this.textNamesBox);
            this.Controls.Add(this.appleIcon);
            this.Controls.Add(this.grassIcon);
            this.Controls.Add(this.addFileButton);
            this.Controls.Add(this.fileNameInput);
            this.Controls.Add(this.folderButton);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::MCJSON.Properties.Settings.Default, "Zom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "App";
            this.Text = global::MCJSON.Properties.Settings.Default.Zom;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.App_FormClosed);
            this.Shown += new System.EventHandler(this.App_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.appleIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grassIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button folderButton;
        private System.Windows.Forms.TextBox fileNameInput;
        private System.Windows.Forms.FolderBrowserDialog modFolderDialog
            ;
        private System.Windows.Forms.Button addFileButton
            ;
        private System.Windows.Forms.PictureBox grassIcon;
        private System.Windows.Forms.PictureBox appleIcon;
        private System.Windows.Forms.Label textNamesBox;
        private System.Windows.Forms.TextBox namesBox;
        private System.Windows.Forms.Label textModFolder;
        private System.Windows.Forms.Button genButton;
        private System.Windows.Forms.Label genCompleteText;
        private System.Windows.Forms.Label textModid;
        private System.Windows.Forms.Button ytButton;
        private System.Windows.Forms.ComboBox blockVariantsBox;
        private System.Windows.Forms.Button removeItemButton;
        private System.Windows.Forms.CheckBox outputFolderCheck;
        private System.Windows.Forms.TextBox cropTextBox;
        private System.Windows.Forms.Label maxStageLabel;
        private System.Windows.Forms.ComboBox itemVariantsBox;
    }
}

